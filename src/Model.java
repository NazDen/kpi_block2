import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Model {
   private int maxN;
   private int minN;
   private int randN;
   private List<Integer> history;

   public Model() {
      this.maxN = 100;
      this.minN = 0;
      history = new ArrayList<>();
      setRandN();
   }

   public void setRandN() {
      this.randN = new Random().nextInt(maxN - 1) + 1;
   }

   public int getMaxN() {
      return maxN;
   }

   public int getMinN() {
      return minN;
   }

   public List getHistory(){
      return this.history;
   }

   public boolean checkNum(int num){
      history.add(num);
      if (num == this.randN) {
         return false;
      }else if (num > this.randN) {
         maxN = num;
      } else {
         minN = num;
      }
      return true;
      }

   public int getRandN() {
      return this.randN;
   }
}
