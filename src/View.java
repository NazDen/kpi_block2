import java.util.List;

public class View {
    public static final String WRONG_INPUT = "Your input is wrong!";

    public void printMessage (String message){
        System.out.println(message);
    }

    public void printInputMessage (int max, int min){
        System.out.println(String.format("You have to guess the number less than %d and more than %d\n"+
                "Please input your number:", max,min));
    }

    public void printCongrMessage (List list){
        System.out.println(String.format("You won! The secret number is %d.\n"+
                "Your numbers:" + list.toString() + ".", list.get(list.size() - 1)));
    }
}
