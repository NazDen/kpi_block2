import java.util.Scanner;

public class Controller {
    Scanner sc = new Scanner(System.in);
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser(){
//        view.printMessage(String.format("Tss.., the secret number is %d", model.getRandN()));
        int num = checkBarriers(getInt());
        while(model.checkNum(num)){
            num = checkBarriers(getInt());
        }
         view.printCongrMessage(model.getHistory());
    }

    private int getInt() {
        view.printInputMessage(model.getMaxN(),model.getMinN());
        String s = sc.nextLine();
        while(!s.matches("^[0-9]+$")){
            view.printMessage(View.WRONG_INPUT);
            view.printInputMessage(model.getMaxN(),model.getMinN());
            s = sc.nextLine();
        }
        return Integer.parseInt(s);
    }

    private int checkBarriers(int num){
        while (num >= model.getMaxN() || num <= model.getMinN()){
            view.printMessage(View.WRONG_INPUT);
            num = getInt();
        }
        return num;
    }
}
